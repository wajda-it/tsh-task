import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Pagination } from './../models/Pagination';
import { PaymentsService } from './../payments.service';
import { Component, OnInit } from '@angular/core';
import { Payment } from '../models/Payment';
import { Filter } from '../models/Filter';
import { PaymentsDetailsComponent } from '../payments-details/payments-details.component';

@Component({
  selector: 'tsh-payments-list',
  templateUrl: './payments-list.component.html',
  styleUrls: ['./payments-list.component.scss']
})
export class PaymentsListComponent implements OnInit {

  payments: Payment[];
  pagination: Pagination;
  links: Object;
  ratings: Array<number>;
  filter: Filter;
  peerPage: number;
 
  constructor(
    private paymentsService: PaymentsService,
    private modalService: NgbModal
  ) {
    this.ratings = [0, 1, 2, 3, 4, 5];
    this.filter = { rating: 0 };
    this.peerPage = 5;
  }

  ngOnInit() {
    this.loadPayments();
  }

  loadPayments(): void {
    this.paymentsService.getPayments()
      .subscribe(
        (data) => {
          this.setVariables(data.payments, data.pagination);
        },
        (err) => {
          this.resetVariables();
        }
      );
  };

  loadPaymentsWithParams(page?: number, query?: string, rating?: string): void {
    this.paymentsService.getPaymentsWithParams(page, query, rating)
      .subscribe(
        (data) => {
          this.setVariables(data.payments, data.pagination);
        },
        (err) => {
          this.resetVariables();
        }
      );
  };

  totalItems(totalPage: number) : number {
    return (+totalPage - 1) * this.peerPage;
  };

  setVariables(payments: Payment[], pagination: Pagination): void {
    this.payments = payments;
    this.pagination = pagination;
    this.pagination.current = +this.pagination.current;
    this.links = Object.keys(this.pagination.links);
  }

  resetVariables(): void {
    this.payments = [];
    this.pagination = {
      total: 0,
      current: 0
    }
  }

  isPayments(payments: Payment[]) : boolean {
    return payments && payments.length > 0;
  }

  isPagination(pagination: Pagination, peerPage: number) : boolean {
    return pagination && pagination.total > peerPage;
  }

  openPaymentDetails(payment) : void {
    const modalRef = this.modalService.open(PaymentsDetailsComponent);
    modalRef.componentInstance.payment = payment;
  }
  
  reset(): void {
    this.filter = { rating: 0 };
    this.pagination.current = 0;
    this.loadPayments();
  }

  search(query?: string, rating?: number) {
    this.loadPaymentsWithParams(0, query, rating.toString());
  }
}
