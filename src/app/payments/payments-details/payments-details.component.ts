import { Component, Input, OnInit } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'tsh-payments-details',
  templateUrl: './payments-details.component.html',
  styleUrls: ['./payments-details.component.scss']
})
export class PaymentsDetailsComponent implements OnInit {
  @Input() payment;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {

  }
}
