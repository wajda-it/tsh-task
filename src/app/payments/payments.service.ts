import { Http, URLSearchParams, RequestOptions } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import { Payments } from './models/Payments';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class PaymentsService {

  private baseUrl: string = "http://test-api.kuria.tshdev.io";
  private apiUrl: string;
  ;

  constructor(private http: Http) { }

  getPayments(): Observable<Payments> {
    return this.http.get(this.baseUrl)
      .map(res => res.json())
      .catch(error => Observable.throw(error.json().message));
  }

  getPaymentsWithParams(page?: number, query?: string, rating?: string): Observable<Payments> {
    this.apiUrl = `${this.baseUrl}?page=${page}&rating=${rating || ''}&query=${query || ''}`;

    return this.http.get(this.apiUrl)
      .map(res => res.json())
      .catch(error => Observable.throw(error.json().message));
  }
}
