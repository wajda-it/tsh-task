export interface Pagination {
    total: number,
    current: number,
    links?: Object,
    from?: number,
    to?: number,
    left?: boolean,
    right?: boolean,
    leftEnd?: boolean,
    rightEnd?: boolean
}