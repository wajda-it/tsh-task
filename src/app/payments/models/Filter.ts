export interface Filter {
    query?: string,
    rating?: number
}