import { Pagination } from './Pagination';
import { Payment } from './Payment';

export interface Payments {
    payments: Payment[],
    pagination: Pagination
}