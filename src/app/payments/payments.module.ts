import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentsListComponent } from './payments-list/payments-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaymentsDetailsComponent } from './payments-details/payments-details.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule   
  ],
  exports: [PaymentsListComponent],
  declarations: [PaymentsListComponent, PaymentsDetailsComponent],
  entryComponents: [PaymentsListComponent, PaymentsDetailsComponent]
})

export class PaymentsModule { }
