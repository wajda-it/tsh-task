import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'

import { AppComponent } from './app.component';
import { PaymentsService } from './payments/payments.service';
import { PaymentsModule } from './payments/payments.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    PaymentsModule,
    NgbModule.forRoot()
  ],
  providers: [PaymentsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
